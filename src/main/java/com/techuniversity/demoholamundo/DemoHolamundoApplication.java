package com.techuniversity.demoholamundo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHolamundoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoHolamundoApplication.class, args);
    }

}
