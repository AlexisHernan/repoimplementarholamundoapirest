package com.techuniversity.demoholamundo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

    @RequestMapping("/saludo")
    public String saludar(){
        return "Hola Tech U!";
    }
}
